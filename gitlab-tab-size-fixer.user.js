// ==UserScript==
// @name        gitlab-tab-size-fixer
// @namespace   gitlab
// @description Gitlab.com tab size fixer
// @include     https://gitlab.com/*
// @include     http://gitlab.com/*
// @version     1
// @grant       none
// ==/UserScript==

var css = 'pre { -moz-tab-size: 4; }';
var elStyle = document.createElement('style');

elStyle.type = 'text/css';
elStyle.appendChild( document.createTextNode(css) );

document.head.appendChild( elStyle );
